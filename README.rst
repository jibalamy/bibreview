BibReview
%%%%%%%%%

BibReview is a software for managing bibliographic database. 
It has originally been developped for `the IMIA Yearbook of Medical Informatics <http://www.imia-medinfo.org/new2/node/110>`_.
It proposes avanced functions for literature reviews, but can also be used as a traditional bibliographic
database manager.

BibReview can import references from PubMed, and export in BibTeX format.
It can also cite reference in `LyX <http://lyx.org>`_.

BibReview requires:

* `Python 2.7 <http://python.org>`_

* `PyGTK <http://pygtk.org>`_

* `EditObj 2 <http://www.lesfleursdunormal.fr/static/informatique/editobj/index_fr.html>`_

You can install it by running :

::
   
   python ./setup.py build
   python ./setup.py install # as root
