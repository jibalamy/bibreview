#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{ae,aecompl}
%\usepackage{hevea}
\usepackage{color}
\usepackage{graphics}
\usepackage{array}
\usepackage{graphicx}

\usepackage{lastpage}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language french
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement t
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 0
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\topmargin 1cm
\rightmargin 1cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes true
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
BibReview
\end_layout

\begin_layout Author
Jean-Baptiste LAMY (LIM&BIO, Université Paris 13, Sorbonne Paris Cité, <
 jibalamy
\begin_inset space ~
\end_inset

@ free.fr >)
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
sloppy
\end_layout

\end_inset


\end_layout

\begin_layout Section
Installation
\end_layout

\begin_layout Subsection
Linux
\end_layout

\begin_layout Standard
Installer Python (version 2.7, pas la version 3) et PyGtk 2 (parfois appelé
 Python-GTK).
 Tout deux sont disponibles dans les paquets de la quasi-totalité des distributi
ons Linux.
\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\begin_inset Note Comment
status collapsed

\begin_layout Plain Layout
Installer editobj2 (en root) :
\end_layout

\begin_layout LyX-Code
easy_install editobj2
\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Télécharger BibReview, le décompresser et l'éxécuter à partir d'un terminal
 de la manière suivante :
\end_layout

\begin_layout LyX-Code
cd <répertoire de BibReview>
\end_layout

\begin_layout LyX-Code
python ./bibreview/bibreview
\end_layout

\begin_layout Subsection
MacOS X
\end_layout

\begin_layout Standard
Installation :
\end_layout

\begin_layout Enumerate
Installer Python (version 2.7.x) si nécessaire uniquement :
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
Vérifier si la version de python locale est bonne.
 Pour cela :
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
ouvrir un terminal (/Applications/Utilitaires/Terminal)
\end_layout

\begin_layout Enumerate
taper : python
\end_layout

\begin_layout Enumerate
si la version est 2.7.x, ne rien faire de plus et passer directement au point
 2.
\end_layout

\end_deeper
\begin_layout Enumerate
Télécharger Python depuis 
\begin_inset CommandInset href
LatexCommand href
target "http://www.python.org/download/"

\end_inset

 en prenant la version pour son système.
\end_layout

\begin_layout Enumerate
Cliquer sur installer.
\end_layout

\begin_layout Enumerate
Sous terminal, vérifier comme en (a).
\end_layout

\end_deeper
\begin_layout Enumerate
Installer PyGTK
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
Télécharger PyGTK.pkg depuis 
\begin_inset CommandInset href
LatexCommand href
target "http://macpkg.sourceforge.net/"

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
L'exécuter en double-cliquant et suivre les instructions d'installation.
\end_layout

\end_deeper
\begin_layout Enumerate
Sous terminal, vérifier en exécutant /opt/gtk/bin/pygtk-demo ; une fenêtre
 doit s'ouvrir.
\end_layout

\end_deeper
\begin_layout Enumerate
Télécharger bibreview et décompresser le fichier .tar.bz2
\end_layout

\begin_layout Standard
Exécution :
\end_layout

\begin_layout Enumerate
Lancement bibreview (GUI) sous terminal :
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout LyX-Code
cd <répertoire de bibreview>
\end_layout

\begin_layout LyX-Code
python ./bibreview/bibreview
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout Standard
[Testé avec Mac OS X 10.5.8 (Leopard), 10.7.5 (Lion), 10.9 (Mavericks) ; en cas
 de problème sur Mac, contacter Jacques Bouaud]
\end_layout

\begin_layout Standard
Astuce de redimensionnement de la fenêtre si les coins du bas ne fonctionnent
 pas, utiliser les coins du haut !
\end_layout

\end_deeper
\begin_layout Subsection
Windows
\end_layout

\begin_layout Standard
Installation :
\end_layout

\begin_layout Enumerate
Télécharger Python 2.7.x (pas la version 3.x) et l'installer dans le répertoire
 C:
\backslash
python27 (celui par défaut) :
\begin_inset Newline newline
\end_inset


\begin_inset CommandInset href
LatexCommand href
target "http://python.org/ftp/python/2.7.3/python-2.7.3.msi"

\end_inset


\end_layout

\begin_layout Enumerate
Télécharger pyGtK 2.24.2 
\begin_inset Quotes eld
\end_inset

all in one installer
\begin_inset Quotes erd
\end_inset

 pour python 2.7 et l'installer (avec les options d'installation par défaut)
 :
\begin_inset Newline newline
\end_inset


\begin_inset CommandInset href
LatexCommand href
target "http://ftp.gnome.org/pub/GNOME/binaries/win32/pygtk/2.24/pygtk-all-in-one-2.24.2.win32-py2.7.msi"

\end_inset


\end_layout

\begin_layout Enumerate
Télécharger bibreview et décompresser le fichier .bz2 puis le fichier .tar,
 dans le répertoire C:
\backslash
bibreview
\end_layout

\begin_layout Standard
Exécution :
\end_layout

\begin_layout Enumerate
Double-cliquer sur le fichier bibreview
\backslash
run_bibreview_windows.py
\end_layout

\begin_layout Standard
Exécution en mode débogage (pour obtenir un message d'erreur en cas de problème)
 :
\end_layout

\begin_layout Enumerate
Cliquer avec le bouton droit sur le fichier bibreview
\backslash
run_bibreview_windows.py, et choisir 
\begin_inset Quotes eld
\end_inset

Edit with IDLE
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Enumerate
Lancer le programme avec le menu Run > Run module (F5)
\end_layout

\begin_layout Section
Utiliser BibReview pour une revue de la littérature
\end_layout

\begin_layout Subsection
Construction et extraction de base bibliographique
\end_layout

\begin_layout Standard
La construction et l'extraction de base se fait 
\series bold
uniquement
\series default
 en ligne de commande.
 L'avantage de la ligne de commande est de garder une trace écrite de la
 procédure utiliser pour l'extraction : il suffit de copier les lignes de
 commande et de les enregistrer dans un fichier.
\end_layout

\begin_layout Standard
Les options disponibles en ligne de commande sont listées dans le tableau
 suivant.
 Dans les options, les paramètres <base> (ou <base1>, <base2>) peuvent être
 soit un nom de fichier XML contenant une base au format BibReview, soit
 le mot clef CURRENT pour utiliser la base en cours (résultant par exemple
 d'une commande précédente).
\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="19" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top" width="10cm">
<row>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Option
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Effet
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
<base>
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Charge la base
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--query-pubmed <requête>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Effectue une requête dans PubMed et crée une nouvelle base à partir du résultat
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--import-pubmed <fichier.xml>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Importe un fichier au format XML extrait de PubMed
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--import-bibtex <fichier.bib>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Importe un fichier au format Bib\SpecialChar TeX

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--save
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Sauvegarde la base sous le même nom de fichier (uniquement disponible si
 la base a été chargé à partir d'un fichier existant)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--save-as <fichier.xml>
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Sauvegarde la base sous le nom <fichier.xml>
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--review-mode <base>
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Passe la base en mode 
\begin_inset Quotes eld
\end_inset

revue
\begin_inset Quotes erd
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--compare <base1> <base2>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Compare en ligne de commande les 2 bases
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--intersection <base1> <base2>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Crée une nouvelle base en faisant l'intersection des 2 bases
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--union <base1> <base2>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Crée une nouvelle base en faisant l'union des 2 bases, 
\series bold
sans
\series default
 fusionner les statuts de revue
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--merge <base1> <base2>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Crée une nouvelle base en faisant l'union des 2 bases, et 
\series bold
en fusionnant
\series default
 les statuts de revue
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--substract <base1> <base2>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Crée une nouvelle base en faisant la différence des 2 bases
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--copy-review-status <base1> <base2>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Copie les statuts de revue de la base 2 vers la base 1 (sans ajouter les
 références manquantes)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--set-review-status <base> <statut>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Met le statut donné à toutes les références de la base 
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--remove-without-author <base>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Enlève de la base toutes les références sans auteur
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--remove-without-abstract <base>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Enlève de la base toutes les références sans abstract
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--remove-without-keyword <base>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Enlève de la base toutes les références sans mot-clef
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
--remove-with-keyword <base>
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Enlève de la base toutes les références avec mot-clef
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Subsection
Revue des articles
\end_layout

\begin_layout Subsubsection
Définir l'identité du relecteur
\end_layout

\begin_layout Standard
Dans les préférerences du logiciel (menu Édition>Préférences), l'option
 
\begin_inset Quotes eld
\end_inset

nom d'utilisateur
\begin_inset Quotes erd
\end_inset

 permet de renseigner le nom sous lequel seront faite vos révisions.
\end_layout

\begin_layout Subsubsection
Passer la base en mode revue
\end_layout

\begin_layout Standard
Si cela n'a pas déjà été fait, il est préférable de passer la base en mode
 revue.
 Ce mode active les raccourcis clavier pour la revue, et affiche par défaut
 dans la liste des étiquettes celles utiles pour la revue (Rejeté, Accepté,
 etc).
\end_layout

\begin_layout Standard
Pour cela, cliquer sur la base tout en haut de la liste à gauche de la fenête,
 puis cocher la case 
\begin_inset Quotes eld
\end_inset

mode revue
\begin_inset Quotes erd
\end_inset

 dans le panneau de droite.
\end_layout

\begin_layout Subsubsection
Effectuer la revue manuelle
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename bibreview_fr.png
	width 80text%

\end_inset


\end_layout

\begin_layout Standard
Une référence peut être acceptée ou rejetée en cochant la case correspondante
 dans les étiquettes du panneau de droite.
 L'historique de revue de la référence (tout en bas du panneau de droite)
 conserve la totalité de l'historique des changements de statut de revue.
\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
En mode revue, les racourcis claviers suivants sont disponibles lorsque
 la liste des articles a le focus :
\end_layout

\begin_layout Description
entré accepter la référence
\end_layout

\begin_layout Description
p marquer la référence comme 
\begin_inset Quotes eld
\end_inset

à revoir
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Description
r rejeter la référence (sans précision)
\end_layout

\begin_layout Description
t rejeter la référence sur le titre
\end_layout

\begin_layout Description
a rejeter la référence sur l'abstract
\end_layout

\begin_layout Description
f rejeter la référence sur le texte (full text)
\end_layout

\begin_layout Description
espace passer à la référence suivante sans modifier son statut
\end_layout

\begin_layout Subsection
Fusion lors de revues avec plusieurs relecteurs
\end_layout

\begin_layout Standard
Lorsque deux relecteurs ont effectué la même revue, il est possible de fusionner
 les revues en ligne de commande avec l'option –merge.
 Lors de la fusion, les statuts de revue sont déterminés à l'aide de la
 matrice suivante :
\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="6">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" bottomline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Rejeté
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
À revoir
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Non relu
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Accepté
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Rejeté
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Rejeté
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
À revoir
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
À revoir
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Non relu
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Non relu
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Accepté
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Conflit
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Accepté
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
De plus, pour les références rejetées, on garde le 
\begin_inset Quotes eld
\end_inset

meilleur
\begin_inset Quotes erd
\end_inset

 rejet : par exemple une référence rejeté sur le titre par le premier relecteur
 et sur l'abstract par le second, sera considérée comme rejeté sur l'abstract.
\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
Lorsque deux relecteurs ont effectué chacun une partie (disjointe) de la
 revue, il est possible de réunir les revues en ligne de commande avec l'option
 –union.
\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
Si plus de deux relecteurs ont effectué la revue, il faut les fusionner
 ou réunir 2 à 2 (il n'est pas possible de fusionner / réunir plus de 2
 bases d'un seul coup).
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Exemple-YB"

\end_inset

Exemple dans le cadre du Yearbook d'informatique médicale
\end_layout

\begin_layout Standard
Pour chaque section du Yearbook, désigner un responsable des extractions
 (en choisissant si possible une personne ayant des compétences en informatique).
 Les étapes suivies d'une (*) sont à réaliser uniquement par ce responsable,
 les autres sont à effectuer par chaque relecteur.
\end_layout

\begin_layout Subsubsection
Création d'un dossier (*)
\end_layout

\begin_layout Standard
Créer un dossier (dans notre exemple /home/jiba/zip/labo/yearbook/2012)
 pour stocker l'ensembles des fichiers pour une année.
\end_layout

\begin_layout Subsubsection
Extraction d'une base partielle (novembre) depuis web of science (*)
\end_layout

\begin_layout Standard
Effectuer dans web of science la requête souhaitée, et exporter le résultat
 au format BibTex et en mode 
\begin_inset Quotes eld
\end_inset

full record
\begin_inset Quotes erd
\end_inset

 dans un fichier nommé 
\begin_inset Quotes eld
\end_inset

wos_partielle_1.bib
\begin_inset Quotes erd
\end_inset

 dans le dossier précédamment créé.
 Web of science ne permet pas d'exporter plus de 500 références à la fois
 ; si nécessaire exporter les 500 premières dans le fichier 
\begin_inset Quotes eld
\end_inset

wos_partielle_1.bib
\begin_inset Quotes erd
\end_inset

, les 500 suivantes dans le fichier 
\begin_inset Quotes eld
\end_inset

wos_partielle_2.bib
\begin_inset Quotes erd
\end_inset

, etc.
\end_layout

\begin_layout Subsubsection
Extraction d'une base partielle (novembre) depuis PubMed (*)
\end_layout

\begin_layout Standard
Le script Python 
\begin_inset Quotes eld
\end_inset

yb_aide_decision.py
\begin_inset Quotes erd
\end_inset

 permet :
\end_layout

\begin_layout Enumerate
d'effectuer la requête PubMed avec mot-clefs MeSH,
\end_layout

\begin_layout Enumerate
d'effectuer la requête PubMed en texte libre,
\end_layout

\begin_layout Enumerate
de combiner les 2 en ne gardant le texte libre que pour les références qui
 n'ont pas encore été indexées avec le MeSH,
\end_layout

\begin_layout Enumerate
de combiner les différents fichier 
\begin_inset Quotes eld
\end_inset

.bib
\begin_inset Quotes erd
\end_inset

 extrait de web of science en un seul fichier, et de l'importer dans BibReview,
\end_layout

\begin_layout Enumerate
de combiner les résultats de Pubmed et de web of science dans une seule
 base.
\end_layout

\begin_layout Enumerate
de fusionner les relectures des différents relecteurs
\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
Le script nécessite d'être adapté à chaque section.
 Pour cela :
\end_layout

\begin_layout Enumerate
Ouvrir le fichier 
\begin_inset Quotes eld
\end_inset

yb_aide_decision.py
\begin_inset Quotes erd
\end_inset

, et l'enregistrer sous 
\begin_inset Quotes eld
\end_inset

yb_<votre_section>.py
\begin_inset Quotes erd
\end_inset

 (NB sous windows, on l'ouvrira en cliquant avec le bouton droit sur le
 fichier puis 
\begin_inset Quotes eld
\end_inset

Edit with IDLE
\begin_inset Quotes erd
\end_inset

).
\end_layout

\begin_layout Enumerate
Renseigner le paramètre 
\begin_inset Quotes eld
\end_inset

annee
\begin_inset Quotes erd
\end_inset

 avec l'année sur laquelle vous travaillez.
\end_layout

\begin_layout Enumerate
Renseigner le paramètre 
\begin_inset Quotes eld
\end_inset

dossier
\begin_inset Quotes erd
\end_inset

 avec le nom du dossier créer précédemment (
\series bold
attention !
\series default
 sous windows, les antislashs 
\begin_inset Quotes eld
\end_inset


\backslash

\begin_inset Quotes erd
\end_inset

 dans le nom du dossier doivent être doublés, par exemple 
\begin_inset Quotes eld
\end_inset

C:
\backslash

\backslash
yearbook
\backslash

\backslash
2012
\begin_inset Quotes erd
\end_inset

).
\end_layout

\begin_layout Enumerate
Renseigner le paramètre 
\begin_inset Quotes eld
\end_inset

bibreview_path
\begin_inset Quotes erd
\end_inset

 avec le chemin vers bibreview.
 Laisser ce paramètre vide si le script est dans le répertoire 
\begin_inset Quotes eld
\end_inset

doc
\begin_inset Quotes erd
\end_inset

 de BibReview.
\end_layout

\begin_layout Enumerate
Renseigner le paramètre 
\begin_inset Quotes eld
\end_inset

q_mesh
\begin_inset Quotes erd
\end_inset

 avec la requête en mots clefs MeSH (NB le script ajoutera automatiquement
 les critères de date, langue et type de publication, donc inutile de les
 mettre).
\end_layout

\begin_layout Enumerate
Renseigner le paramètre 
\begin_inset Quotes eld
\end_inset

q_text
\begin_inset Quotes erd
\end_inset

 avec la requête en texte libre.
\end_layout

\begin_layout Standard
Exécuter le script (NB sous windows, utiliser le menu Run > Run module).
\end_layout

\begin_layout Standard
La base résultante s'appelle 
\begin_inset Quotes eld
\end_inset

yb_partielle.xml
\begin_inset Quotes erd
\end_inset

.
 Il est conseillé d'ouvrir cette base avec BibReview et de vérifier l'absence
 d'erreur ; on pourra par exemple utiliser les menus Édition > Analyser
 la fréquence des journaux / des derniers auteurs pour vérifier que l'on
 retrouve bien les principaux journaux de la discipline et auteurs 
\begin_inset Quotes eld
\end_inset

seniors
\begin_inset Quotes erd
\end_inset

.
\end_layout

\begin_layout Standard
Enfin, duppliquer ce fichier en 
\begin_inset Quotes eld
\end_inset

yb_partielle_<nom_du_relecteur>.xml
\begin_inset Quotes erd
\end_inset

 (un nom différent pour chaque relecteur), et envoyer chaque fichier à son
 relecteur.
\end_layout

\begin_layout Subsubsection
Revue manuelle partielle (novembre - décembre)
\end_layout

\begin_layout Standard
Renseigner votre nom dans le logiciel, afin de 
\begin_inset Quotes eld
\end_inset

tracer
\begin_inset Quotes erd
\end_inset

 qui a accepté ou rejeté quoi lors de la relecture (menu Édition > Préférences).
\end_layout

\begin_layout Standard
Ouvrir le fichier 
\begin_inset Quotes eld
\end_inset

yb_partielle_<votre_nom>.xml
\begin_inset Quotes erd
\end_inset

 dans BibReview et effectuer la revue.
 Penser à enregistrer régulièrement et à sauvegarder votre travail, on ne
 sait jamais...!
\end_layout

\begin_layout Standard
Une fois la revue effectuée, envoyer le fichier avec les revues au responsable.
\end_layout

\begin_layout Subsubsection
Fusion des bases partielles (*)
\end_layout

\begin_layout Standard
Enregistrer les fichiers provenant des différentes revues dans le dossier
 du Yearbook.
\end_layout

\begin_layout Standard
Réexécuter le script.
 Celui va fusionner les bases partielles des différents relecteurs dans
 la base yb_partielle_fusion.xml.
\end_layout

\begin_layout Subsubsection
Extraction d'une base complète (tout 2012) depuis web of science (*)
\end_layout

\begin_layout Standard
Au début de l'année suivante, refaire la requête dans web of science, et
 exporter les résultats dans le fichier 
\begin_inset Quotes eld
\end_inset

wos_complete_1.bib
\begin_inset Quotes erd
\end_inset

 (et 2, etc, si nécessaire).
\end_layout

\begin_layout Subsubsection
Extraction d'une base complète (tout 2012) depuis PubMed (*)
\end_layout

\begin_layout Standard
Réexécuter le script Python pour refaire la requête Pubmed (NB le script
 détecte automatiquement que nous avons changé d'année).
\end_layout

\begin_layout Standard
La base résultante s'appelle 
\begin_inset Quotes eld
\end_inset

yb_complete.xml
\begin_inset Quotes erd
\end_inset

.
 Les différentes bases 
\begin_inset Quotes eld
\end_inset

yb_complete_<nom_des_relecteurs>.xml
\begin_inset Quotes erd
\end_inset

 comprennent les résultats des revues partielles.
 Elles sont à envoyées aux différents relecteurs.
\end_layout

\begin_layout Subsubsection
Revue manuelle complète (janvier)
\end_layout

\begin_layout Standard
Ouvrir le fichier 
\begin_inset Quotes eld
\end_inset

yb_complete_<votre_nom>.xml
\begin_inset Quotes erd
\end_inset

 dans BibReview et effectuera la revue.
\end_layout

\begin_layout Standard
Astuce : on triera les références par statut de revue (menu Édition>Trier
 par statut de revue) pour isoler toutes les références qui n'ont pas encore
 été revues.
\end_layout

\begin_layout Standard
Une fois la revue effectuée, envoyer le fichier avec les revues au responsable.
\end_layout

\begin_layout Subsubsection
Fusion des revues (*)
\end_layout

\begin_layout Standard
Réexécuter le script.
 Celui va fusionner les bases complètes des différents relecteurs dans la
 base yb_complete_fusion.xml.
\end_layout

\begin_layout Subsubsection
Analyser les résultats
\end_layout

\begin_layout Standard
Ouvrir le fichier 
\begin_inset Quotes eld
\end_inset

yb_complete_fusion.xml
\begin_inset Quotes erd
\end_inset

 dans BibReview et interpréter les accords / désaccords...
\end_layout

\end_body
\end_document
