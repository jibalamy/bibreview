# -*- coding: utf-8 -*-

# BibReview
# Copyright (C) 2019 Jean-Baptiste LAMY (jibalamy at free . fr)
# BibReview is developped by Jean-Baptiste LAMY, at LIM&BIO,
# UFR SMBH, Université Paris 13, Sorbonne Paris Cité.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



import sys, os, time, uno

#UNO_CONNEXION = "uno:socket,host=%s,port=%s;urp;"
UNO_CONNEXION = sys.argv[1] or "uno:pipe,name=ooo_pipe;urp;"
FIELDS        = sys.argv[2:]

PARAGRAPH_BREAK = uno.getConstantByName("com.sun.star.text.ControlCharacter.PARAGRAPH_BREAK")
DIRECT_VALUE    = 0 #uno.getConstantByName("com.sun.star.beans.PropertyState.DIRECT_VALUE")
PropertyValue   = uno.getClass("com.sun.star.beans.PropertyValue")

OO_FIELDS = [
  'Identifier', 'BibiliographicType', 'Address', 'Annote', 'Author',
  'Booktitle', 'Chapter', 'Edition', 'Editor', 'Howpublished', 'Institution',
  'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher',
  'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1',
  'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN']
#OO_FIELD_2_ID = { field : i for i, field in enumerate(OO_FIELDS)}



CONNECTION = None
class Connection(object):
  def __init__(self):
    global CONNECTION
    CONNECTION = self
    self.local_context = uno.getComponentContext()
    self.resolver      = self.local_context.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", self.local_context)
    try:
      self.context     = self.resolver.resolve("%sStarOffice.ComponentContext" % UNO_CONNEXION)
    except:
      os.system("libreoffice --accept='pipe,name=ooo_pipe;urp;' &")
      time.sleep(0.5)
      self.context     = self.resolver.resolve("%sStarOffice.ComponentContext" % UNO_CONNEXION)
    self.manager       = self.context.ServiceManager
    self.desktop       = self.manager.createInstanceWithContext("com.sun.star.frame.Desktop", self.context)
    self.transformer   = self.manager.createInstanceWithContext("com.sun.star.util.URLTransformer", self.context)
    
    
    
def cite(ref):
  if not CONNECTION: Connection()
  
  model = CONNECTION.desktop.getCurrentComponent()
  if model and model.getImplementationName() == 'SwXTextDocument': # current document is a text document
    controller = model.getCurrentController()
    cursor     = controller.getViewCursor()
    text       = model.Text
    bibFound = False
    
    for i in range(model.getDocumentIndexes().getCount()):
      biblio = model.getDocumentIndexes().getByIndex(i)
      if biblio.getServiceName() == 'com.sun.star.text.Bibliography': break
    else:
      end_cursor = text.createTextCursor()
      end_cursor.gotoEnd(False)
      text.insertControlCharacter(end_cursor, PARAGRAPH_BREAK, False)
      biblio = model.createInstance("com.sun.star.text.Bibliography")
      text.insertTextContent(end_cursor, biblio, False)
      if text.compareRegionEnds(cursor, end_cursor) == 0:  # the viewcursor was at the end of the doc
        cursor.gotoRange(biblio.getAnchor().getStart(), False)	# we must relocate it before the index
        cursor.goLeft(1, False)
      biblio.update()
      
      
    c = cursor.Text.createTextCursorByRange(cursor)	# Add at cursor location (replace selection)
    oo_ref = model.createInstance("com.sun.star.text.TextField.Bibliography")
    oo_ref.Fields = [ PropertyValue(k, 0, v, DIRECT_VALUE)
                      for k, v in zip(OO_FIELDS, FIELDS) ]
    #print(oo_ref.Fields)
    c.Text.insertTextContent(c, oo_ref, True)
    biblio.update()
    
    
    
if __name__ == "__main__":
  cite(None)
